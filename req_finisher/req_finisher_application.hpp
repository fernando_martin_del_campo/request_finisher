#include "../toe/toe.hpp"

void req_finisher(   volatile ap_uint<64> * Data_in,
				volatile unsigned * num_of_mems,
				volatile unsigned * mem_size,
				ap_uint<5> * state_out,
				unsigned * done_read_requests,
				unsigned * done_write_requests
);
