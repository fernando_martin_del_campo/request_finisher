#include "req_finisher_application.hpp"


void client( volatile ap_uint<64> * Data_in,
			 volatile unsigned * num_of_mems,
			 volatile unsigned * mem_size,
             ap_uint<5> * state_out,
             unsigned * done_read_requests,
             unsigned * done_write_requests
			 )
{
#pragma HLS PIPELINE II=10
#pragma HLS INLINE off
	// Reads new data from memory and writes it into fifo
	// Read & write metadata only once per package
	static ap_uint<5> esac_fsmState = 16;

	static ap_uint<64> header;
	static ap_uint<64> new_get;                     //to read if there's a valid request to send back
	static bool read_req;
	static unsigned num_mems = *num_of_mems;
	static unsigned bram_offset = *mem_size;
	static unsigned current_mem = 0;
	static unsigned count_read_acc;
	static unsigned count_write_acc;
	static unsigned current_offset = 0;
	*state_out = esac_fsmState;

	switch (esac_fsmState)
	{
	case 16:
	{
		num_mems = *num_of_mems;
		bram_offset = *mem_size >> 3;
		current_mem = 0;
		count_read_acc = 0;
		count_write_acc = 0;

		esac_fsmState = 0;

		break;
	}
	case 0:
	{
		current_offset = 1 + current_mem*bram_offset;

		esac_fsmState = 5;

		break;
	}
	case 5:
	{
		*( Data_in + current_offset ) = 1;
		++current_mem;

		if  ( current_mem == num_mems)
		{
			esac_fsmState = 1;
		}
		else
		{
			esac_fsmState = 0;
		}

	    break;
	}
	case 1:
	{
		if (current_mem == num_mems)
		{
			current_mem = 0;
		}

		current_offset = 1 + current_mem*bram_offset;
		esac_fsmState = 2;

	    break;
	}
	case 2:
	{

		new_get = *( Data_in + current_offset);

		esac_fsmState = 3;
		break;
	}
	case 3:
	{
		if ( new_get == 0 )
		{
			current_offset = current_mem*bram_offset;
			esac_fsmState = 4;
		}
		else
		{
			++current_mem;
			esac_fsmState = 1;
		}

		break;
	}
	case 4:
	{
		header = *(Data_in + current_offset);

		esac_fsmState = 6;

		break;
	}
	case 6:                                           //see if there's a new request to finish and see if it's a read or a write
	{
	   current_offset = 1 + current_mem*bram_offset;
       if ( header.range(31,16) < 42 )           //read
       {
     	   read_req = true;
       }
       else                                       //write
       {
     	  read_req = false;
       }
       esac_fsmState = 9;

		break;
	}
	case 9:
	{
		*(Data_in + current_offset ) = 1;
		++current_mem;
		esac_fsmState = 1;

		if (read_req)
		{
			++count_read_acc;
		}
		else
		{
			++count_write_acc;
		}
		*done_read_requests = count_read_acc;
		*done_write_requests = count_write_acc;
		break;
	}}
}

void req_finisher(	volatile ap_uint<64> *      Data_in,
				volatile unsigned        *      num_of_mems,
				volatile unsigned        *      mem_size,
				ap_uint<5>               *      state_out,
				unsigned                 *      done_read_requests,
				unsigned                 *      done_write_requests
				)
{
#pragma HLS DATAFLOW
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE ap_ovld port=state_out
#pragma HLS INTERFACE ap_none port=done_read_requests
#pragma HLS INTERFACE ap_none port=done_write_requests
#pragma HLS INTERFACE ap_none port=num_of_mems
#pragma HLS INTERFACE ap_none port=mem_size

#pragma HLS INTERFACE m_axi depth=100 port=Data_in

	client( Data_in, num_of_mems, mem_size, state_out, done_read_requests, done_write_requests );

}
