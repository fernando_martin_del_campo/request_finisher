/************************************************
Copyright (c) 2016, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors 
may be used to endorse or promote products derived from this software 
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.// Copyright (c) 2015 Xilinx, Inc.
************************************************/
#include "tcp_out_application.hpp"

int main()
{
	ap_uint<64> Data_in[10];
	Data_in[0]= 0x0000000000090001;
	Data_in[1]= 0x010101abcdefaa01;
	Data_in[2]= 0x0000000000000001;

	//ap_uint<64> Data_p = 0;
	//Data_in = &Data_p;

	hls::stream<bool> readyReq;
	hls::stream<appTxMeta> txMetaData;
	hls::stream<axiWord> txData;
	hls::stream<bool> done_out;

	ap_uint<2> *state_out;
	ap_uint<2> state_ptr = 6;
    state_out = &state_ptr;

	readyReq.write(false);

	//tcp_out(	Data_in, readyReq,
	//							txMetaData, txData,
	//							done_out, state_out);

	//std::cout << std::dec << "State: " << *state_out << std::endl;

	readyReq.write(true);


	tcp_out(	Data_in, readyReq,
										txMetaData, txData,
										done_out, state_out);

	std::cout << std::endl << std::dec << "State: " << *state_out << std::endl;
	return 0;
}
