#ifndef LENGTH_CONFIG_H
#define LENGTH_CONFIG_H

#define LARGE_SIZE              80  //the equivalent of 10 32-bits words. Over that value requests go directly to main memory.
#define BRAM_SIZE             2048  //8K BRAMS
#define BRAM_NUM                 1  //number of TCP_IN requests BRAMS. Should change with the prismatic generation scripts
#define MEM_SIZE            524288  //2 MB for large requests going directly to main memory

#endif

