open_project prj_req_finisher

set_top req_finisher

add_files req_finisher_application.cpp
add_files -tb test_req_finisher_application.cpp

open_solution "solution1"
set_part {xcvu37p-fsvh2892-2-e-es1}
create_clock -period 2.4 -name default

config_rtl -disable_start_propagation
#csynth_design
#export_design -format ip_catalog -display_name "Echo Server Application for 10G TOE" -description "Echos packets on connections coming in on port 7." -vendor "ethz.systems" -version "1.2"
exit
