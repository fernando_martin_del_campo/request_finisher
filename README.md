Request finisher
=====

A module that performs the functions of the TCP out module for Syn-Prismatic

Compatibility
---

The system is designed in Vivado HLS 2019.1

Build Request finisher
---

```
cd req_finisher
```

```
vivado_hls -f run_hls.tcl
```

```
vivado_hls -p prj_req_finisher
```

C-syhthesize and generate RTL
